/*jshint esversion: 8 */

export let RouteList = [
	(await import("/js/modules/Connect.js")).Connect,
	(await import("/js/modules/user/User.js")).User,
	(await import("/js/modules/bot/Bot.js")).Bot,
	(await import("/js/modules/bot/Indicator.js")).Indicator,
	(await import("/js/modules/bot/Strategy.js")).Strategy,
	(await import("/js/modules/bot/Test.js")).Test,
];

export let EndpointList = [
	(await import("/js/modules/user/User.js")).User,
	(await import("/js/modules/bot/Bot.js")).Bot,
	(await import("/js/modules/bot/Indicator.js")).Indicator,
	(await import("/js/modules/bot/Strategy.js")).Strategy,
	(await import("/js/modules/bot/Test.js")).Test,
];
