/*jshint esversion: 11 */
import {BaseModal} from "/js/modals/BaseModal.js";

export class IndicatorModal extends BaseModal {
	constructor(type, data=null) {
		super(
			"indicator",
			type === "create" ? "Create indicator" : "Update indicator",
			"closable",
			true
		);

		const button = document.querySelector("dialog#modal form button.submit");
		button.innerHTML = type === "create" ? "Create" : "Update";

		const form = document.querySelector("dialog#modal form");
		const name = document.querySelector("input#indicator-name");
		const version = document.querySelector("input#indicator-version");
		const is_public = document.querySelector("input#indicator-public");
		const file = document.querySelector("dialog#modal form input[type=\"file\"]");
		const editor = document.querySelector("dialog#modal form textarea");
		file.addEventListener("change", (f) => {
			Array.from(file.files).forEach(file => {
				const reader = new FileReader();
				reader.onload = (e) => { editor.value = e.target.result; };
				reader.readAsText(file);
			});
		});

		let old_code = null;
		let old_version = "0.0.0";
		let id = null;
		if(type === "update") {
			name.value = data.indicator.name;
			version.value = data.indicator.version;
			old_version = data.indicator.version;
			is_public.checked = data.indicator.public;
			editor.value = data.indicator.code;
			old_code = data.indicator.code;
			id = parseInt(location.pathname.split("/").filter(e => e!=="").at(-2)) || null;
		}

		button.addEventListener("click", e => {
			if(!form.reportValidity())
				return;
			if (type === "update" && editor.value !== old_code && !this.valid_version(version.value, old_version)) {
				//notify error
				return;
			}
			const data = {
				indicator: {
					name: name.value,
					public: is_public.checked
				},
			};
			const v = version.value.split(".");
			if(editor.value !== old_code)
				data.version = {
					code: editor.value,
					major: parseInt(v[0]),
					minor: parseInt(v[1]),
					revision: parseInt(v[2])
				};
			if(type === "create")
				fetch(window.app.endpoint + "/indicator/create", {
					method: "POST",
					mode: 'cors',
					headers: {"Content-Type": "application/json"},
					body: JSON.stringify(data)
				}).then(r => r.json()).then((d) => {
					if(d.status)
						this.hide(true);
					else {
						// show error
					}
				});
			else {
				data.indicator.id = id;
				fetch(window.app.endpoint + "/indicator/update", {
					method: "PUT",
					mode: 'cors',
					headers: {"Content-Type": "application/json"},
					body: JSON.stringify(data)
				}).then(r => r.json()).then((d) => {
					if(d.status)
						this.hide(true);
					else {
						// show error
					}
				});
			}
		});
	}

	valid_version(version, old_version) {
		if(!(/^\d+\.\d+\.\d+$/.test(version)))
			return false;
		version = version.split(".");
		old_version = old_version.split(".");
		for(let i in old_version) {
			if(old_version[i] > (version[i] || 0))
				return false;
		}
		return true;
	}
}
