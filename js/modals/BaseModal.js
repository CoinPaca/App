/*jshint esversion: 11 */

export class BaseModal {
	static #dialog;
	static #header;
	static #container;
	static #template;
	#modifiers = "";

	constructor(template, header="", modifier="") {
		BaseModal.#template = window.app.modals[template];
		if(!BaseModal.#dialog) {
			BaseModal.#dialog = document.querySelector("dialog#modal");
			BaseModal.#header = document.querySelector("dialog#modal .container header");
			BaseModal.#container = document.querySelector("dialog#modal .container aside");
		}
		BaseModal.#header.innerHTML = header;
		const close = document.createElement("button");
		close.addEventListener("click", e => {
			this.hide();
		});
		BaseModal.#header.appendChild(close);
		BaseModal.#dialog.className = "";
		this.#modifiers = modifier.split(" ").filter(e => e!=="");
		this.show();
	}

	show() {
		BaseModal.#container.innerHTML = BaseModal.#template;
		BaseModal.#dialog.setAttribute("open", "");
		for(let i in this.#modifiers)
			BaseModal.#dialog.classList.add(this.#modifiers[i]);
	}

	hide(reload=false) {
		BaseModal.#dialog.removeAttribute("open");
		BaseModal.#container.innerHTML = "";
		for(let i in this.#modifiers)
			BaseModal.#dialog.classList.remove(this.#modifiers[i]);
		const route = JSON.parse(localStorage.getItem("route") || "{}");
			if(route.referer && route.referer.url)
				history.replaceState(route.referer.state, "", route.referer.url, !reload);
	}
}

export class ConfirmModal extends BaseModal {
	constructor(message, title, callback, fallback=null, modifier="") {
		super("confirm", title || "Confirm", "small closable confirm " + modifier);
		const container = document.querySelector("dialog#modal .message");
		container.innerHTML = message;
		const confirm = document.querySelector("dialog#modal button.confirm");
		const cancel = document.querySelector("dialog#modal button.cancel");
		confirm.addEventListener("click", () => {callback(); this.hide(true);});
		cancel.addEventListener("click", fallback || (() => this.hide()));
	}

}
