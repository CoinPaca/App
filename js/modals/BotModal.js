/*jshint esversion: 11 */
import {BaseModal} from "/js/modals/BaseModal.js";

export class BotModal extends BaseModal {
	constructor(type, data=null) {
		super("bot", type === "add" ? "Add service" : "Edit service", "small closable", true);

		const button = document.querySelector("dialog#modal form button.submit");
		button.innerHTML = type === "add" ? "Add" : "Edit";

		const form = document.querySelector("form#bot-form");
		const name = document.querySelector("input#bot-name");
		const host = document.querySelector("input#bot-host");
		const port = document.querySelector("input#bot-port");
		const serverSecret = document.querySelector("input#bot-server-secret");
		const typeSelect = document.querySelector("select#bot-type-select");
		const id = parseInt(location.pathname.split("/").filter(e => e!=="").at(-1)) || null;

		if(type === "edit") {
			name.value = data.bot.name;
			host.value = data.bot.host;
			port.value = data.bot.port;
			serverSecret.value = data.bot.server_secret;
			typeSelect.value = data.bot.type;
		}

		button.addEventListener("click", e => {
			if(!form.reportValidity())
				return;
			const data = {
				name: name.value,
				host: host.value,
				port: parseInt(port.value),
				server_secret: serverSecret.value,
				type: parseInt(typeSelect.value)
			};

			if(type === "add")
				fetch(window.app.endpoint + "/bot/create", {
					method: "POST",
					mode: 'cors',
					headers: {"Content-Type": "application/json"},
					body: JSON.stringify(data)
				}).then(r => r.json()).then((d) => {
					if(d.status)
						this.hide(true);
					else {
						// show error
					}
				});
			else {
				data.id = id;
				fetch(window.app.endpoint + "/bot/update", {
					method: "PUT",
					mode: 'cors',
					headers: {"Content-Type": "application/json"},
					body: JSON.stringify(data)
				}).then(r => r.json()).then((d) => {
					if(d.status)
						this.hide(true);
					else {
						// show error
					}
				});
			}
		});
	}
}
