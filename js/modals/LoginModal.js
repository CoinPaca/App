/*jshint esversion: 11 */

import {BaseModal} from "/js/modals/BaseModal.js";

export class LoginModal extends BaseModal {
	constructor() {
		super("login", "Login", "small");

		let username = document.querySelector("#modal input#login-username");
		let password = document.querySelector("#modal input#login-password");
		let button = document.querySelector("#modal button#login-send");

		button.addEventListener("click", () => {
			if (username.value && password.value)
				fetch(window.app.endpoint + "/login", {
					method: "POST",
					mode: 'cors',
					headers: {"Content-Type": "application/json"},
					body: JSON.stringify({
						username: username.value,
						password: password.value,
					})
				}).catch(e => {
					app.notiifer.error("<b>Connection error</b><br />Cannot connect to API server.");
				});
		});
	}
}
