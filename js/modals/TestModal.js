/*jshint esversion: 11 */
import {BaseModal} from "/js/modals/BaseModal.js";

export class TestModal extends BaseModal {
	constructor(options) {
		super("test", "Run test", "small closable");
		this.pairs = [];

		this.testAllPairs = document.querySelector("dialog#modal form input#test-all-pairs");
		this.runOn = document.querySelector("dialog#modal form select#test-run-on");
		this.tradingStart = document.querySelector("dialog#modal form input#test-from");
		this.tradingEnd = document.querySelector("dialog#modal form input#test-to");

		const button = document.querySelector("dialog#modal form button.submit");
		button.innerHTML = "Run";

		this.strategies(options.strategies);
		this.pairs_init(options.pairs);
		this.funds(options.assets);
		this.services(options.services);

		button.addEventListener("click", () => { this.process(); });
	}

	strategies(strategies) {
		const strategies_container = document.querySelector("dialog#modal form .test-strategies");
		const strategy = document.querySelector("dialog#modal form select.strategy");
		const version = document.querySelector("dialog#modal form select.version");
		const add_strategy = document.querySelector("dialog#modal form button#add-test-strategy");
		let used = [];

		const insertStrategies = (strategy) => {
			for(let s in strategies) {
				if(used.includes(s) || strategy.value === s)
					continue;
				const option = document.createElement("option");
				option.value = s;
				option.innerHTML = strategies[s].name;
				strategy.appendChild(option);
			}
		};

		const change = (value, version) => {
			if(version) {
				version.value = "";
				version.disabled = false;
				version.innerHTML = "";
				version.required = true;
				const option_version = document.createElement("option");
				option_version.innerHTML = "Version";
				option_version.selected = true;
				option_version.disabled = true;
				version.appendChild(option_version);
				for(let i in strategies[value].versions) {
					const option = document.createElement("option");
					option.value = i;
					option.innerHTML = strategies[value].versions[i];
					version.appendChild(option);
				}
			}
			used = [];
			const elems = document.querySelectorAll("dialog#modal form select.strategy");
			for(let elem of elems)
				if(elem.value)
					used.push(elem.value);

			for(let elem of elems) {
				Array.from(elem.children).forEach(c => {
					if(c.value && c.value !== c.parentNode.value) {
						c.parentNode.removeChild(c);
					}
				});
				insertStrategies(elem);
			}
		};

		insertStrategies(strategy);
		strategy.addEventListener("change", () => change(strategy.value, version));
		add_strategy.addEventListener("click", e => {
			e.preventDefault();
			if(document.querySelectorAll("dialog#modal form select.strategy").length >= Object.values(strategies).length)
				return;
			const container = document.createElement("div");
			const select_strategy = document.createElement("select");
			const select_version = document.createElement("select");
			const remove = document.createElement("button");
			const option_strategy = document.createElement("option");
			option_strategy.innerHTML = "Strategy";
			option_strategy.selected = true;
			option_strategy.disabled = true;
			select_strategy.classList.add("strategy");
			select_strategy.appendChild(option_strategy);
			select_strategy.required = true;
			const option_version = document.createElement("option");
			option_version.innerHTML = "Version";
			option_version.selected = true;
			option_version.disabled = true;
			select_version.classList.add("version");
			select_version.appendChild(option_version);
			select_version.disabled = true;
			select_version.required = true;
			remove.classList.add("icon");
			remove.classList.add("remove");
			remove.innerHTML = "&#xF146;";
			container.classList.add("test-strategy");

			container.appendChild(select_strategy);
			container.appendChild(select_version);
			container.appendChild(remove);
			strategies_container.appendChild(container);

			insertStrategies(select_strategy);
			select_strategy.addEventListener("change", () => change(select_strategy.value, version));
			remove.addEventListener("click", e => {
				e.preventDefault();
				if(select_strategy.value)
					used = used.filter(e => e !== select_strategy.value);
				container.parentNode.removeChild(container);
				change();
			});
		});
	}

	pairs_init(pairs) {
		const all_pairs = document.querySelector("dialog#modal form input#test-all-pairs");
		const pair = document.querySelector("dialog#modal form input#test-pair");
		const add = document.querySelector("dialog#modal form button#test-pair-add");
		const list = document.querySelector("dialog#modal form #test-pair-list");
		all_pairs.addEventListener("change", () => {
			if(all_pairs.checked) {
				pair.disabled = true;
				add.disabled = true;
				list.classList.add("hidden");
			}
			else {
				pair.disabled = false;
				add.disabled = false;
				list.classList.remove("hidden");
			}
		});
		const insert = e => {
			e.preventDefault();
			const values = pair.value.toUpperCase().split(/[, ]/i);
			for(let value of values) {
				if(!value || !pairs[value] || this.pairs.includes(pairs[value]))
					continue;
				this.pairs.push(pairs[value]);
				const tag = document.createElement("span");
				tag.innerHTML = value;
				tag.dataset.id = pairs[value];
				list.appendChild(tag);
				tag.addEventListener("click", () => {
					list.removeChild(tag);
					this.pairs = this.pairs.filter(e => e !== parseInt(tag.dataset.id));
				});
			}
			pair.value = "";
		};

		add.addEventListener("click", insert);
		pair.addEventListener("keypress", e => { if(e.key === "Enter") insert(e); });
	}

	funds(assets) {
		const asset = document.querySelector("dialog#modal form .fund select.asset");
		const amount = document.querySelector("dialog#modal form .fund input.amount");
		const add = document.querySelector("dialog#modal form button#test-funds-add");
		const funds_container = document.querySelector("dialog#modal form .test-funds");

		let used = [];


		const insertValues = (asset) => {
			for(let a in assets) {
				if(used.includes(a) || asset.value === a)
					continue;
				const option = document.createElement("option");
				option.value = a;
				option.innerHTML = assets[a];
				asset.appendChild(option);
			}
		};

		const change = (amount) => {
			if(amount)
				amount.disabled = false;
			used = [];
			const elems = document.querySelectorAll("dialog#modal form select.asset");
			for(let elem of elems)
				if(elem.value)
					used.push(elem.value);

			for(let elem of elems) {
				Array.from(elem.children).forEach(c => {
					if(c.value && c.value !== c.parentNode.value) {
						c.parentNode.removeChild(c);
					}
				});
				insertValues(elem);
			}
		};

		insertValues(asset);
		asset.addEventListener("change", () => change(amount));
		add.addEventListener("click", e => {
			e.preventDefault();
			if(document.querySelectorAll("dialog#modal form select.asset").length >= Object.values(assets).length)
				return;
			const container = document.createElement("div");
			const asset = document.createElement("select");
			const amount = document.createElement("input");
			const remove = document.createElement("button");

			const option = document.createElement("option");
			option.value = "";
			option.innerHTML = "Asset";
			option.disabled = true;
			option.selected = true;
			asset.classList.add("asset");
			asset.appendChild(option);
			asset.required = true;

			amount.type = "number";
			amount.classList.add("amount");
			amount.placeholder = "Amount";
			amount.min = 0;
			amount.step = "any";
			amount.required = true;
			amount.disabled = true;

			remove.classList.add("icon");
			remove.classList.add("remove");
			remove.innerHTML = "&#xF146;";

			container.appendChild(asset);
			container.appendChild(amount);
			container.appendChild(remove);
			funds_container.appendChild(container);

			insertValues(asset);
			asset.addEventListener("change", () => change(amount));
			remove.addEventListener("click", e => {
				e.preventDefault();
				if(asset.value)
					used = used.filter(e => e !== asset.value);
				container.parentNode.removeChild(container);
				change();
			});
		});
	}

	services(services) {
		for(let service in services) {
			let option = document.createElement("option");
			option.value = service;
			option.innerHTML = services[service];
			this.runOn.appendChild(option);
		}
	}

	process() {
		if(!this.checkValidity())
			return;

		fetch(window.app.endpoint + "/test/run", {
			method: "POST",
			mode: 'cors',
			headers: {"Content-Type": "application/json"},
			body: JSON.stringify(this.prepareData())
		}).then(r => r.json()).then((d) => {
			console.log("test run response", d);
			if(d.status)
				this.hide(true);
			else {
				// show error
			}
		});
	}

	checkValidity() {
		const form = document.querySelector("dialog#modal form");
		const amounts = document.querySelectorAll("dialog#modal form input.amount");
		const strategy_versions = document.querySelectorAll("dialog#modal form select.version");
		const checkDateInterval = (start, end) => {
			if(!start)
				return true;
			start = start.split("-").map(e => { return parseInt(e); });
			if(end) {
				end = end.split("-").map(e => { return parseInt(e); });
				for(let i in start)
					if(start[i] > end[i])
						return false;
			}
			const n = new Date();
			return start[0] <= n.getFullYear() && start[1] <= n.getMonth()+1 && start[2] <= n.getDate();
		};

		if(!form.reportValidity())
			return false;

		for(let i=0; i < strategy_versions.length; i++)
			if(isNaN(parseInt(strategy_versions[i].value)))
				return false;

		for(let i in amounts) {
			try {
				if(parseFloat(amounts[i].value) <= 0)
					return false;
			}
			catch(e) {
				return false;
			}
		}

		if(!this.testAllPairs.checked && !this.pairs.length)
			return false;

		if(!checkDateInterval(this.tradingStart.value, this.tradingEnd.value))
			return false;

		return true;
	}

	prepareData() {
		const strategies_select = document.querySelectorAll("dialog#modal form select.strategy");
		const strategy_versions = document.querySelectorAll("dialog#modal form select.version");

		const assets = document.querySelectorAll("dialog#modal form select.asset");
		const amounts = document.querySelectorAll("dialog#modal form input.amount");

		const strategies = {};
		for(let i=0; i < strategies_select.length; i++)
			strategies[strategies_select[i].value] = parseInt(strategy_versions[i].value);

		const funds = {};
		for(let i=0; i < assets.length; i++)
			funds[assets[i].value] = parseFloat(amounts[i].value);

		const data = {
			"test": {
				"service_id": parseInt(this.runOn.value)
			},
			"strategies": Object.values(strategies),
			"funds": funds,
		};
		if(!this.testAllPairs.checked)
			data.pairs = this.pairs;
		if(this.tradingStart.value)
			data.test.date_start = this.tradingStart.value;
		if(this.tradingEnd.value)
			data.test.date_stop = this.tradingEnd.value;
		console.log("sending data: ", data);
		return data;
	}
}
