/*jshint esversion: 11 */
import {TestModal} from "/js/modals/TestModal.js";
import {ConfirmModal} from "/js/modals/BaseModal.js";

export class Test {
	static list(r) {
		document.querySelector("button#test-run").addEventListener("click", () => {
			window.history.pushState(null, "", "/tests/run");
		});
	}
	static run() {
		fetch(window.app.endpoint + `/test/options`).then(r => {
			r.json().then(d => {
				new TestModal(d.options);
			});
		});
	}
	static detail() {}

	static router() {
		const list = window.route.get("/tests");
		const run = window.route.get("/tests/run");
		const detail = window.route.get(/^\/tests\/detail\/\d+\/?$/);

		list.addEventListener("load", () => { fetch(window.app.endpoint + "/tests"); });
		run.addEventListener("load", () => { Test.run(); });
		detail.addEventListener("load", () => {
			const parts = location.pathname.split("/");
			const id = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
			fetch(window.app.endpoint + "/test/" + id);
		});
	}

	static endpoint() {
		const tests = window.endpoint.get(window.app.endpoint + "/tests");
		const detail = window.endpoint.get(new RegExp("^" + window.app.endpoint + "/test/\\d+/?$"));
		tests.addEventListener("load", (r) => Test.list(r), "GET");
		tests.addEventListener("load", (r) => Test.detail(r), "GET");
	}
}
